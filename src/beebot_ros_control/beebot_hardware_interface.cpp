#include <sstream>
#include <beebot_ros_control/beebot_hardware_interface.h>

#define pi 3.1415926

MyRobot::MyRobot() : 
	sim_trans_r(200.0, 1.0),
	sim_trans_l(200.0, 1.0)
{ 
	//Hardware Interfaces
	//joint state handle and interface
	hardware_interface::JointStateHandle jnt_state_handle_r("right_wheel_joint", &a_curr_pos[0], &a_curr_vel[0], &a_curr_eff[0]);
	hardware_interface::JointStateHandle jnt_state_handle_l("left_wheel_joint", &a_curr_pos[1], &a_curr_vel[1], &a_curr_eff[1]);
	jnt_state_interface.registerHandle(jnt_state_handle_r);
	jnt_state_interface.registerHandle(jnt_state_handle_l);

	registerInterface(&jnt_state_interface);

	//joint velocity handle and interface
	hardware_interface::JointHandle vel_handle_r(jnt_state_interface.getHandle("right_wheel_joint"), &j_cmd_vel[0]);
	hardware_interface::JointHandle vel_handle_l(jnt_state_interface.getHandle("left_wheel_joint"), &j_cmd_vel[1]);

	jnt_vel_interface.registerHandle(vel_handle_r);
	jnt_vel_interface.registerHandle(vel_handle_l);

	registerInterface(&jnt_vel_interface);


	// Transmission Interfaces
	// Wrap simple transmission raw data - current state
	// the right wheel
	a_state_data[0].position.push_back(&a_curr_pos[0]);
	a_state_data[0].velocity.push_back(&a_curr_vel[0]);
	a_state_data[0].effort.push_back(&a_curr_eff[0]);

	j_state_data[0].position.push_back(&j_curr_pos[0]);
	j_state_data[0].velocity.push_back(&j_curr_vel[0]);
	j_state_data[0].effort.push_back(&j_curr_eff[0]);
	
	// the left wheel
	a_state_data[1].position.push_back(&a_curr_pos[1]);
	a_state_data[1].velocity.push_back(&a_curr_vel[1]);
	a_state_data[1].effort.push_back(&a_curr_eff[1]);

	j_state_data[1].position.push_back(&j_curr_pos[1]);
	j_state_data[1].velocity.push_back(&j_curr_vel[1]);
	j_state_data[1].effort.push_back(&j_curr_eff[1]);


	// Wrap simple transmission raw data - velocity command
	a_cmd_data[0].velocity.push_back(&a_cmd_vel[0]); 
	j_cmd_data[0].velocity.push_back(&j_cmd_vel[0]); 
	
	a_cmd_data[1].velocity.push_back(&a_cmd_vel[1]); 
	j_cmd_data[1].velocity.push_back(&j_cmd_vel[1]); 

	// Register transmissions to each interface
	act_to_jnt_state.registerHandle(transmission_interface::ActuatorToJointStateHandle("sim_trans_r", &sim_trans_r, a_state_data[0], j_state_data[0]));
	act_to_jnt_state.registerHandle(transmission_interface::ActuatorToJointStateHandle("sim_trans_r", &sim_trans_l, a_state_data[1], j_state_data[1]));

	jnt_to_act_vel.registerHandle(transmission_interface::JointToActuatorVelocityHandle("sim_trans_r", &sim_trans_r, a_cmd_data[0], j_cmd_data[0]));
	jnt_to_act_vel.registerHandle(transmission_interface::JointToActuatorVelocityHandle("sim_trans_l", &sim_trans_l, a_cmd_data[1], j_cmd_data[1]));
}

MyRobot::~MyRobot()
{
}

double MyRobot::read(SerialHandle *my_port) 
{
	//double l_old_vel = 0.0;
	//double r_old_vel = 0.0;

	// l_curr_vel and r_curr_vel are in "rad/s"
	double l_curr_vel = a_cmd_vel[0] * pi/30;
	double r_curr_vel = a_cmd_vel[1] * pi/30;

	a_curr_pos[0] += l_curr_vel * this->getPeriod().toSec();  
	a_curr_pos[1] += r_curr_vel * this->getPeriod().toSec();  
#if 0
	std::cout << "====pose===" << std::endl;
	std::cout << "l_pose = " << a_curr_pos[0] << std::endl;
	std::cout << "r_pose = " << a_curr_pos[1] << std::endl;
	std::cout << std::endl;
#endif
#if 0
	static double counter = 0.1;
	// Read actuator state from hardware
	//a_curr_vel[0] = a_cmd_vel[0];
	//a_curr_vel[1] = a_cmd_vel[1];
	a_curr_pos[0] = 1.0 + counter;
	a_curr_pos[1] = 1.0 + counter;

	// Propagate current actuator state to joints
	//act_to_jnt_state.propagate();	

	std::cout << "----- Read ---------"  << std::endl;
	std::cout << "right joint vel = " << j_cmd_vel[0] << std::endl;
	std::cout << "left joint vel = "  << j_cmd_vel[1] << std::endl;
	std::cout << std::endl;

	counter += 1.0; 
#endif
}

double MyRobot::write(SerialHandle *my_port)
{
	// Porpagate joint commands to actuators
	jnt_to_act_vel.propagate();

	// Write data to serial port
	std::stringstream ssr, ssl;
	ssr << "2v" <<  a_cmd_vel[0] << "\r";
	ssl << "1v" << -a_cmd_vel[1] << "\r";
		
	my_port->writeData(ssr.str());
	my_port->writeData(ssl.str());

	// Show on the terminal
	std::cout << "----- Write --------"  << std::endl;
	std::cout << "right joint vel = " << j_cmd_vel[0] << std::endl;
	std::cout << "left joint vel = "  << j_cmd_vel[1] << std::endl;
	std::cout << "right actuator vel = " << ssr.str() << std::endl;
	std::cout << "left actuator vel = "  << ssl.str() << std::endl;
	std::cout << "--------------------"  << std::endl;
	std::cout << std::endl;

	// Send actuator command to hardware
	// ...
}

ros::Time MyRobot::getTime() const 
{
	return ros::Time::now();
}

ros::Duration MyRobot::getPeriod() const 
{
	return ros::Duration(0.1);
}

