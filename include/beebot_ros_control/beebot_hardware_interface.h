#ifndef BEEBOT_HARDWARE_INTERFACE
#define BEEBOT_HARDWARE_INTERFACE

#include <iostream>
#include <ros/ros.h>
#include <ros/time.h>
#include <ros/duration.h>
#include <controller_manager/controller_manager.h>
#include <hardware_interface/joint_command_interface.h>
#include <hardware_interface/joint_state_interface.h>
#include <hardware_interface/robot_hw.h>
#include <transmission_interface/simple_transmission.h>
#include <transmission_interface/transmission_interface.h>
#include <beebot_ros_control/beebot_serial.h>


class MyRobot : public hardware_interface::RobotHW
{
public:
	MyRobot();
	~MyRobot();
	double read(SerialHandle *);
	double write(SerialHandle *);
	ros::Time getTime() const;
	ros::Duration getPeriod() const;

private:
	//Hardware interface;
	hardware_interface::JointStateInterface jnt_state_interface;
	hardware_interface::VelocityJointInterface jnt_vel_interface;

	//Transmission interfaces
	transmission_interface::ActuatorToJointStateInterface    act_to_jnt_state;
	transmission_interface::JointToActuatorVelocityInterface jnt_to_act_vel;

	//Transmissions
	transmission_interface::SimpleTransmission sim_trans_r;
	transmission_interface::SimpleTransmission sim_trans_l;

	// Actuator and joint space variables: wrappers around raw data
	transmission_interface::ActuatorData a_state_data[2];
	transmission_interface::ActuatorData a_cmd_data[2];

	transmission_interface::JointData j_state_data[2];
	transmission_interface::JointData j_cmd_data[2];

	// Actuator and joint space variables - raw data:
	// The two actuators/joints are coupled through a reducer.
	double a_curr_pos[2]; 
	double a_curr_vel[2];
	double a_curr_eff[2];
	double a_cmd_vel[2];

	double j_curr_pos[2]; 
	double j_curr_vel[2];
	double j_curr_eff[2];
	double j_cmd_vel[2];
};

#endif
