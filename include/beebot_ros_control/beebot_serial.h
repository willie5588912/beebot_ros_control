#ifndef BEEBOT_SERIAL
#define BEEBOT_SERIAL

#include <sstream>
#include <SerialStream.h>

using namespace LibSerial ;    

class SerialHandle
{
	public:
		SerialHandle(const char* const p_port_name);
		~SerialHandle();
		void set_baudRate(int p_baud_rate);
		void set_dataBits(int p_data_bits);
		void set_stopBit(int p_stop_bit);
		void set_parity(bool p_parity);
		void set_hardwareFlowControl(bool p_control);
		void writeData(std::string p_data);
		
	private:
    	SerialStream serial_port;
		int _baud_rate;
		int _data_bits;
		int _stop_bit;
		bool _flow_control;
};

#endif
